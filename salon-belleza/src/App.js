import banner  from './assets/images/banner.jpg'; // Tell webpack this JS file uses this image
import './App.css';

function App() {
  return (
    <div className="App">
      <nav className='nav'>
        <div className='title'>Salon de belleza </div>
        
        <div className='button-container'> 
          <button  className= 'button-nav'onClick="handleClick()"> Inicio </button>

          <button  className= 'button-nav'onClick="handleClick()"> Acerca de nosotros2 </button>

          <button  className= 'button-nav'onClick="handleClick()"> Productos </button>

          <button  className= 'button-nav'onClick="handleClick()"> Registrarse </button>

          <button  className= 'button-nav'onClick="handleClick()"> Iniciar Sesión</button>
        </div>
      </nav>
      <div className='images-container'>
        <img className='image' src={banner}></img>
      </div>
    </div>
  );
}

export default App;
